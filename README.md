# dnswitch
A low performance DNS proxy/switch/router/whatever.

## overview

Runs as a local resolver that proxies out DNS requests to different upstream resolvers based on query names.

Sample `/etc/dnswitch.conf`:

    [general]
    listen_address = 127.0.0.2:53
    
    [pools]
    default = 8.8.8.8 8.8.4.4
    dnscrypt = 127.0.0.1:53
    behind_some_vpn = 10.10.0.23 10.10.0.24
    
    [domains]
    dnscrypt =
        pornhub.com
    
    behind_some_vpn =
        work.boring.com
        morework.boring.com

In this example, 
- `dnswitch` will listen on `127.0.0.2:53`. 
- any DNS requests for `pornhub.com` round-robin through the `dnscrypt` pool
- any DNS queries for `work.boring.com` or `morework.boring.com` will round-robin through the `behind_some_vpn` pool
- and any other queries not defined will round-robin through the `default` pool.

The `default` pool is required.

## installation options
- [PKGBUILD](https://gitlab.com/jdiemz/dnswitch-PKGBUILD) for Arch.
- Clone the repo and run `python setup.py install`


## syntax
    usage: dnswitch [options] [-l HOOKS] [-c CONFIG]
    
      -c CONFIG, --config CONFIG        Config file. [default: dnswitch.conf]
      -l HOOKS, --log-hooks HOOKS       Special logging hooks to enable.  These messages are
                                        logged
                                        at level INFO.
                                        Available hooks:
                                            data          - Dump full request/response
                                            dispatch      - Query routing
                                            error         - Decoding error
                                            recv          - Raw packet received
                                            reply         - DNS Response
                                            request       - DNS Request
                                            send          - Raw packet sent
                                            truncated     - Truncated
                                        [default: error,request,reply,truncated,dispatch]
      --log-file FILE                   If set, write to log file instead of stdout.
      --log-level LEVEL                 Logging level.  [default: INFO]
      -h, --help
      -v, --version
