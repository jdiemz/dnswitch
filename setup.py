#!/usr/bin/env python

from setuptools import setup, find_packages
from codecs import open
from os import path


here = path.abspath(path.dirname(__file__))

setup(
    name='dnswitch',
    version='0.1',
    description='A low performance DNS proxy/switch',
    long_description='A low performance DNS proxy/switch',
    url='https://gitlab.com/jdiemz/dnswitch',
    author='Jay D Iemz',
    author_email='jdiemz@gmail.com',
    packages=find_packages(exclude=['contrib', 'docs', 'tests']),
    install_requires=['docopt', 'dnslib'],
    data_files = [('/usr/bin', ['stubs/dnswitch'])]
)
