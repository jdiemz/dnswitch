#!/usr/bin/env python

import configparser
import os


class Config(configparser.ConfigParser):
    def load(self, filename, search_path=None):
        if not search_path:
            search_path = ['./', '/etc/']
        fname = False
        r = False
        for p in search_path:
            fname = p + filename
            try:
                r = os.stat(fname)
            except:
                pass
            if r:
                break
        self.read(fname)


config = Config()
