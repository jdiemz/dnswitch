#!/usr/bin/env python
"""usage: dnswitch [options] [-l HOOKS] [-c CONFIG]

  -c CONFIG, --config CONFIG        Config file. [default: dnswitch.conf]
  -l HOOKS, --log-hooks HOOKS       Special logging hooks to enable.  These messages are
                                    logged
                                    at level INFO.
                                    Available hooks:
                                        data          - Dump full request/response
                                        dispatch      - Query routing
                                        error         - Decoding error
                                        recv          - Raw packet received
                                        reply         - DNS Response
                                        request       - DNS Request
                                        send          - Raw packet sent
                                        truncated     - Truncated
                                    [default: error,request,reply,truncated,dispatch]
  --log-file FILE                   If set, write to log file instead of stdout.
  --log-level LEVEL                 Logging level.  [default: INFO]
  -h, --help
  -v, --version
"""

import docopt
import dnslib
from dnslib import DNSRecord
from dnslib.label import DNSLabel
from dnslib.server import BaseResolver
from dnslib.server import DNSHandler
from dnslib.server import DNSServer
import logging
import socket
import sys
import time

from dnswitch import config
from dnswitch import ArrogantDNSLogger
from dnswitch import UpstreamPool


LOG = logging.getLogger('dnswitch')


class SwitchResolver(BaseResolver):
    def __init__(self, pools, mappings):
        """
            pools = {'upstream_name': UpstreamPool, ...}
            mappings = {DNSLabel('domain.name'): 'upstream_name', ...}
        """
        self.pools = pools
        self.mappings = mappings

    def resolve(self, request, handler):
        qname = request.q.qname
        upstream = None
        for d, m in self.mappings.items():
            if qname.matchSuffix(d):
                upstream = m
                break
        if not upstream:
            upstream = 'default'

        address, port = next(self.pools[upstream])
        handler.server.logger.log_dispatch(handler, request, upstream, address, port)

        try:
            response = request.send(address, port, timeout=5)
            reply = DNSRecord.parse(response)
            self.pools[upstream].incr((address, port))
            return reply
        except socket.timeout as e:
            LOG.warning('Upstream %s timed out', (address, port))
            raise e


def main():
    args = docopt.docopt(__doc__, version="0.1")
    config.load(args['--config'])

    ## Configure global logger
    formatter = logging.Formatter('%(asctime)s %(levelname)s [%(name)s] %(message)s')
    if args['--log-file']:
        fh = logging.FileHandler(args['--log-file'])
        fh.setFormatter(formatter)
        LOG.addHandler(fh)
    else:
        sh = logging.StreamHandler()
        sh.setFormatter(formatter)
        LOG.addHandler(sh)
    LOG.setLevel(args['--log-level'])

    ## dnslib specific logger
    dnslib_logger = ArrogantDNSLogger(args['--log-hooks'])

    ## Read in upstream pools
    pools = {}
    for pool_name, servers in config['pools'].items():
        pools[pool_name] = UpstreamPool(servers.split())
    if not 'default' in pools:
        LOG.critical("error: default pool not specified in config")
        sys.exit(1)

    ## Read in domain -> upstream mappings
    mappings = {}
    for pool_name, domains in config['domains'].items():
        if pool_name not in pools:
            LOG.error("error: pool %s referenced but not defined. skipping", pool_name)
            continue
        for d in domains.split():
            label = DNSLabel(d)
            mappings[label] = pool_name

    resolver = SwitchResolver(pools, mappings)

    listen_address, listen_port = config['general']['listen_address'].split(':')
    listen_port = int(listen_port)

    udp_server = DNSServer(address = listen_address,
                           port = listen_port,
                           resolver = resolver,
                           logger = dnslib_logger)
    LOG.info("Starting thread")
    udp_server.start_thread()
    while udp_server.isAlive():
        time.sleep(1)


if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        pass
