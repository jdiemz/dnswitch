#!/usr/bin/env python

import binascii
from dnslib import QTYPE
from dnslib import RCODE
from dnslib import RR
import logging


LOG = logging.getLogger('dnswitch.' + __name__)


class ArrogantDNSLogger(object):
    def __init__(self, log = None, level = 'INFO'):
        if not log:
            enabled = ['request', 'reply', 'truncated', 'error', 'dispatch']
        else:
            enabled = log.split(',')

        for f in dir(self):
            if f.startswith('log_') and callable(getattr(self, f)) and f[4:] not in enabled:
                setattr(self, f, self.noop)

        self.level = getattr(logging, level)

    def noop(self, *args):
        pass

    def prefix(self, handler):
        return "[%s:%s] " % (handler.__class__.__name__,
                             handler.server.resolver.__class__.__name__)

    def log_recv(self, handler, data):
        LOG.log(self.level, "%sReceived: [%s:%d] (%s) <%d> : %s",
                    self.prefix(handler),
                    handler.client_address[0],
                    handler.client_address[1],
                    handler.protocol,
                    len(data),
                    binascii.hexlify(data))

    def log_send(self, handler, data):
        LOG.log(self.level, "%sSent: [%s:%d] (%s) <%d> : %s",
                    self.prefix(handler),
                    handler.client_address[0],
                    handler.client_address[1],
                    handler.protocol,
                    len(data),
                    binascii.hexlify(data))

    def log_request(self, handler, request):
        LOG.log(self.level, "%sRequest: [%s:%d] (%s) / '%s' (%s)",
                    self.prefix(handler),
                    handler.client_address[0],
                    handler.client_address[1],
                    handler.protocol,
                    request.q.qname,
                    QTYPE[request.q.qtype])
        self.log_data(handler, request)

    def log_reply(self, handler, reply):
        if reply.header.rcode == RCODE.NOERROR:
            LOG.log(self.level, "%sReply: [%s:%d] (%s) / '%s' (%s) / RRs: %s",
                    self.prefix(handler),
                    handler.client_address[0],
                    handler.client_address[1],
                    handler.protocol,
                    reply.q.qname,
                    QTYPE[reply.q.qtype],
                    ",".join([QTYPE[a.rtype] for a in reply.rr]))
        else:
            LOG.log(self.level, "%sReply: [%s:%d] (%s) / '%s' (%s) / %s",
                    self.prefix(handler),
                    handler.client_address[0],
                    handler.client_address[1],
                    handler.protocol,
                    reply.q.qname,
                    QTYPE[reply.q.qtype],
                    RCODE[reply.header.rcode])
        self.log_data(handler, reply)

    def log_truncated(self, handler, reply):
        LOG.log(self.level, "%sTruncated Reply: [%s:%d] (%s) / '%s' (%s) / RRs: %s",
                    self.prefix(handler),
                    handler.client_address[0],
                    handler.client_address[1],
                    handler.protocol,
                    reply.q.qname,
                    QTYPE[reply.q.qtype],
                    ",".join([QTYPE[a.rtype] for a in reply.rr]))
        self.log_data(handler, reply)

    def log_error(self, handler, e):
        LOG.log(self.level, "%sInvalid Request: [%s:%d] (%s) :: %s",
                    self.prefix(handler),
                    handler.client_address[0],
                    handler.client_address[1],
                    handler.protocol,
                    e)

    def log_dispatch(self, handler, request, upstream, address, port):
        LOG.log(self.level, "%sDispatching: '%s' (%s) -> pool '%s' (%s:%s)",
                    self.prefix(handler),
                    request.q.qname,
                    QTYPE[request.q.qtype],
                    upstream,
                    address,
                    port)

    def log_data(self, handler, reply):
        LOG.log(self.level, "%s\n%s\n", self.prefix(handler), reply.toZone("    "))
