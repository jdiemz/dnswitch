#!/usr/bin/env python

import logging


LOG = logging.getLogger('dnswitch.' + __name__)


class UpstreamPool(object):
    def __init__(self, servers):
        """
            servers = ['127.0.0.1:53', '127.0.0.2', ...]
        """
        self.servers = []
        self.server_data = {}

        for s in servers:
            t = s.split(':')
            if len(t) == 1:
                addr, port = t[0], 53
            else:
                addr, port = t[0], int(t[1])
            self.servers.append((addr, port))
            self.server_data[(addr, port)] = {'attempts': 0, 'answers': 0}

        self.len = len(self.servers)
        self.current = 0

    def __iter__(self):
        return self

    def __next__(self):
        next_server = self.servers[self.current]
        self.server_data[next_server]['attempts'] += 1
        self.current = (self.current + 1) % self.len
        return next_server

    def __str__(self):
        output = ""

        max_name_len = 22
        max_att_len = 8
        max_ans_len = 7
        for k, v in self.server_data.items():
            name_len = len(k[0] + str(k[1]))
            if name_len > max_name_len:
                max_name_len = name_len

            att_len = len(str(v['attempts']))
            if att_len > max_att_len:
                max_att_len = att_len

            ans_len = len(str(v['answers']))
            if ans_len > max_ans_len:
                max_ans_len = ans_len

        format_string = "{:<%d} {:>%d} {:>%d}\n" % (max_name_len + 2, max_att_len + 2, max_ans_len + 2)
        output += format_string.format("servers", "attempts", "answers")
        for k, v in self.server_data.items():
            addr = "{}:{}".format(k[0], k[1])
            output += format_string.format(addr, v['attempts'], v['answers'])

        return output

    def incr(self, server):
        try:
            self.server_data[server]['answers'] += 1
        except KeyError:
            LOG.warning("UpstreamPool.incr: server %s doesn't exist", server)
